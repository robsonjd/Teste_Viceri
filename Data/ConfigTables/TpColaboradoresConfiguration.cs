﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ConfigTables
{

    public partial class TpColaboradoresConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TpColaboradores>
    {
        //public ColaboradorConfiguration()
        //   // : this("dbo")
        //{
        //}

        public TpColaboradoresConfiguration()//string schema)
        {
            // ToTable(schema + ".Colaborador");
            ToTable("TPCOLABORADORES");
            HasKey(x => new { x.Id });

            Property(x => x.Tipo).HasColumnName("TIPO").IsOptional().HasColumnType("varchar").HasMaxLength(50);
          

        }
    }
}
