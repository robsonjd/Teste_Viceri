﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.ConfigTables
{

    public partial class ColaboradorConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Colaborador>
    {
        public ColaboradorConfiguration()//string schema)
        {
            ToTable("COLABORADOR");
            HasKey(x => new { x.IdColaborador });
            Property(x => x.IdColaborador).HasColumnName("IDCOLABORADOR").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Nome).HasColumnName("NOME").IsOptional().HasColumnType("varchar").HasMaxLength(100);
            Property(x => x.Email).HasColumnName("EMAIL").IsOptional().HasColumnType("varchar").HasMaxLength(255);
            Property(x => x.IdTipo).HasColumnName("IDTIPO").IsOptional().HasColumnType("char").HasMaxLength(1);
            Property(x => x.Cpf).HasColumnName("CPF").IsOptional().HasColumnType("varchar").HasMaxLength(14);
            Property(x => x.Fl_Ativo).HasColumnName("FL_ATIVO").IsOptional().HasColumnType("char").HasMaxLength(1);
        }
    }
}
