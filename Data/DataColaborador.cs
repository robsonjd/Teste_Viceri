﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Data
{
    public class DataColaborador
    {

        public List<ListagemColaborador> GetListagemColaboradores()
        {
            var ListagemColaborador = new List<ListagemColaborador>();

            using (var db = new DBConexao())
            {
                ListagemColaborador = (from a in db.Colaborador
                                       select new ListagemColaborador()
                                       {
                                           Nome = a.Nome,
                                           Email = a.Email,
                                           Cpf = a.Cpf,
                                           IdTipo = a.IdTipo,
                                           IdColaborador = a.IdColaborador,
                                           Fl_Ativo = a.Fl_Ativo
                                       }
                                        ).ToList();

            }

            return ListagemColaborador;
        }

        public List<SelectListItem> GetTipoColaboradores()
        {
            List<SelectListItem> l = new List<SelectListItem>();

            using (var db = new DBConexao())
            {
                l = (from a in db.TpColaboradores
                     select new SelectListItem
                     {
                         Text = a.Tipo,
                         Value = a.Id.ToString()
                     }).ToList();
            }

            l.Insert(0, new SelectListItem() { Text = "", Value = "" });

            return l;

        }

        public Colaborador GetColaborador(int id)
        {
            using (var db = new DBConexao())
            {
                return db.Colaborador.Where(x => x.IdColaborador == id).FirstOrDefault();
            }
        }

        public void Incluir(Colaborador objTela)
        {
            using (var db = new DBConexao())
            {
                var queryMax = db.Colaborador.AsNoTracking();
                int seq = (queryMax.Count() > 0 ? (queryMax.Max(x => x.IdColaborador) + 1) : 1);

                objTela.IdColaborador = seq;

                db.Colaborador.Add(objTela);
                db.SaveChanges();

            }
        }

        public void Alterar(int id, Colaborador objTela)
        {
            using (var db = new DBConexao())
            {
                Colaborador objBanco = this.GetColaborador(id);

                objBanco.Cpf = objTela.Cpf;
                objBanco.Email = objTela.Email;
                objBanco.IdTipo = objTela.IdTipo;
                objBanco.Nome = objTela.Nome;

                db.Colaborador.Attach(objBanco);

                db.Entry(objBanco).Property(x => x.Cpf).IsModified = true;
                db.Entry(objBanco).Property(x => x.Email).IsModified = true;
                db.Entry(objBanco).Property(x => x.IdTipo).IsModified = true;
                db.Entry(objBanco).Property(x => x.Nome).IsModified = true;

                db.SaveChanges();

            }
        }

        public void AtivarReativar(int id, string tipoStatus)
        {
            using (var db = new DBConexao())
            {
                Colaborador objBanco = this.GetColaborador(id);

                if (tipoStatus == "D")
                    objBanco.Fl_Ativo = "D";
                else
                    objBanco.Fl_Ativo = "A";


                db.Colaborador.Attach(objBanco);

                db.Entry(objBanco).Property(x => x.Fl_Ativo).IsModified = true;

                db.SaveChanges();

            }
        }

    }
}
