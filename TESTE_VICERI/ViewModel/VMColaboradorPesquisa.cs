﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Data;

namespace TESTE_VICERI.ViewModel
{
    public class VMColaboradorPesquisa //: IValidatableObject
    {



        [Display(Name = "Tipo do Colaborador")]
        public int? TipoColaboradorSelecionado { get; set; }
        public List<SelectListItem> TipoColaborador { get; set; }


        [StringLength(100)]
        public string Nome { get; set; }


        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(14)]
        public string CPF { get; set; }


        public List<ListagemColaborador> Listagem { get; set; }

        public String DescricaoTipoColaborador(string idTipo)
        {
            string descricao = string.Empty;

            DBConexao dbc = new DBConexao();
                descricao = dbc.TpColaboradores.Where(x => x.Id.ToString() == idTipo).FirstOrDefault().Tipo;

            return descricao;

        }

        
    }
}