﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using Data;
using Model.Commun;

namespace TESTE_VICERI.ViewModel
{
    public class VMColaboradorEdit : IValidatableObject
    {
        public VMColaboradorEdit()
        {

        }

        public int? id { get; set; }

        [Required]
        [Display(Name = "Tipo do Colaborador")]
        public int? TipoColaboradorSelecionado { get; set; }
        public List<SelectListItem> TipoColaborador { get; set; }


        [Required]
        [StringLength(100)]
        public string Nome { get; set; }

        
        [Required]
        [StringLength(255)]
        [EmailAddress(ErrorMessage = "Email inválido")]
        public string Email { get; set; }


        [Required]
        [StringLength(14)]
        public string CPF { get; set; }


        public List<ListagemColaborador> Listagem { get; set; }

        public String DescricaoTipoColaborador(string idTipo)
        {
            string descricao = string.Empty;
            using (var db = new DBConexao())
            {
                descricao = db.TpColaboradores.Where(x => x.Id.ToString() == idTipo).FirstOrDefault().Tipo;
            }
            return descricao;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Nome.Length > 100)
                yield return new ValidationResult("O Campo nome deve conter no máximo 100 caracteres", null);

            if (Email.Length > 255)
                yield return new ValidationResult("O campo Email deve conter no máximo 255 caracteres", null);
            else
            {
                if (!this.id.HasValue &&  !string.IsNullOrEmpty(Email))
                {
                    using (var db = new DBConexao())
                    {
                        if (db.Colaborador.Where(x => x.Email == this.Email).Count() > 0)
                        {
                            yield return new ValidationResult("Email já cadastrado", null);
                        }
                    }
                }
            }

            if (TipoColaboradorSelecionado == 0)
                yield return new ValidationResult("O campo Tipo do Colaborador deve ser selecionado", null);

            if (!string.IsNullOrEmpty(CPF))
            {
                if (!Cpf.CPFValido(CPF))
                {
                    yield return new ValidationResult("CPF inválido", null);
                }
                else
                {
                    if (!this.id.HasValue)
                    {
                        using (var db = new DBConexao())
                        {
                            if (db.Colaborador.Where(x => x.Cpf.Replace(".", "").Replace("/", "") == this.CPF.Replace(".", "").Replace("/", "")).Count() > 0)
                            {
                                yield return new ValidationResult("Cpf já cadastro", null);
                            }
                        }
                    }
                }
            }

        }

    }
}