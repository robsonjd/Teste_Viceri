﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TESTE_VICERI.ViewModel;

namespace TESTE_VICERI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Colaborador()
        {
            VMColaboradorEdit view = new VMColaboradorEdit();
            return RedirectToAction("Index" , "InicialColaborador");
            
        }
    }
}