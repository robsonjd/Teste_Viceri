﻿using Data;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TESTE_VICERI.ViewModel;

namespace TESTE_VICERI.Controllers
{
    public class InicialColaboradorController : Controller
    {
        private DBConexao db = new DBConexao();
        // GET: InicialColaborador
        public ActionResult Index()
        {
            VMColaboradorPesquisa view = new VMColaboradorPesquisa();
            DataColaborador dataColaborador = new DataColaborador();
            view.Listagem = dataColaborador.GetListagemColaboradores();
            view.TipoColaborador = dataColaborador.GetTipoColaboradores();
            return View("Index", view);

        }

        [HttpPost]
        public ActionResult Pesquisa(VMColaboradorPesquisa view)
        {
            DataColaborador dataColaborador = new DataColaborador();
            view.TipoColaborador = dataColaborador.GetTipoColaboradores();
            view.Listagem = RetornaPesquisa(view); //dataColaborador.GetListagemColaboradores();
            return View("Index", view);

        }

        private List<ListagemColaborador> RetornaPesquisa(VMColaboradorPesquisa view)
        {
            var query = (from a in db.Colaborador
                         select a);
            //r por Nome, E-mail, CPF e Tipo do Colaborador.
            if (!String.IsNullOrEmpty(view.Nome))
                query = query.Where(x => x.Nome.ToUpper().Contains(view.Nome.ToUpper()));
            if (!String.IsNullOrEmpty(view.Email))
                query = query.Where(x => x.Email.ToUpper().Contains(view.Email.ToUpper()));
            if (!String.IsNullOrEmpty(view.CPF) && !String.IsNullOrEmpty(view.CPF.Replace(".", "").Replace("/", "")))
                query = query.Where(x => x.Cpf.Replace(".", "").Replace("/", "").Contains(view.CPF.Replace(".", "").Replace("/", "")));
            if (view.TipoColaboradorSelecionado.HasValue)
                query = query.Where(x => x.IdTipo == view.TipoColaboradorSelecionado.ToString());

            view.Listagem = query.Select(a => new ListagemColaborador()
            {
                Nome = a.Nome,
                Email = a.Email,
                Cpf = a.Cpf,
                IdTipo = a.IdTipo,
                IdColaborador = a.IdColaborador
            }
                                        ).ToList();


            return view.Listagem;



        }


        public ActionResult Edit()//int id
        {
            return RedirectToAction("Index", "Home");
        }

        public JsonResult AtivarDesativar(string id, string tipoStatus)
        {
            DataColaborador dataColaborador = new DataColaborador();
           
            dataColaborador.AtivarReativar(Convert.ToInt32(id), tipoStatus);

            return Json(new
            {
                Response = "OK"
            });
        }



        public ActionResult Voltar()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Novo()
        {
            return RedirectToAction("Index", "Colaborador");
        }
    }
}