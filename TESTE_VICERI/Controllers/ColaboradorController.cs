﻿using Data;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TESTE_VICERI.ViewModel;

namespace TESTE_VICERI.Controllers
{
    public class ColaboradorController : Controller
    {

        public ActionResult Index(VMColaboradorEdit view)
        {
            DataColaborador dataColaborador = new DataColaborador();

            view.Listagem = dataColaborador.GetListagemColaboradores();

            view.TipoColaborador = dataColaborador.GetTipoColaboradores();
            ModelState.Clear();
            return View(view);
        }

        // GET: Colaborador/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Colaborador/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(VMColaboradorEdit view)
        {
            DataColaborador dataColaborador = new DataColaborador();
            view.Listagem = dataColaborador.GetListagemColaboradores();
            view.TipoColaborador = dataColaborador.GetTipoColaboradores();

            if (!ModelState.IsValid)
                return View("Index", view);

            var obj = new Colaborador()
            {
                Nome = view.Nome,
                Cpf = view.CPF,
                Email = view.Email,
                IdTipo = view.TipoColaboradorSelecionado.ToString(),
                Fl_Ativo = "A"
                
            };

            dataColaborador.Incluir(obj);

            return RedirectToAction("Index", "InicialColaborador");

        }

        public ActionResult Voltar(VMColaboradorEdit viewr)
        {
            return RedirectToAction("Index", "InicialColaborador");
        }

        // GET: Colaborador/Edit/5
        public ActionResult Edit(int id)
        {

            VMColaboradorEdit view = new VMColaboradorEdit();
            DataColaborador dataColaborador = new DataColaborador();
            view.Listagem = dataColaborador.GetListagemColaboradores();
            view.TipoColaborador = dataColaborador.GetTipoColaboradores();

            var objBanco = dataColaborador.GetColaborador(id);

            view.CPF = objBanco.Cpf;
            view.Email = objBanco.Email;
            view.Nome = objBanco.Nome;
            view.TipoColaboradorSelecionado = Convert.ToInt32(objBanco.IdTipo);
            view.id = objBanco.IdColaborador;

            ModelState.Clear();

            return View(view);


        }

        // POST: Colaborador/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, VMColaboradorEdit view)
        {

            DataColaborador dataColaborador = new DataColaborador();
            view.Listagem = dataColaborador.GetListagemColaboradores();
            view.TipoColaborador = dataColaborador.GetTipoColaboradores();

            if (!ModelState.IsValid)
                return View("Index", view);

            Colaborador objTela = new Colaborador()
            {
                Cpf = view.CPF,
                IdTipo = view.TipoColaboradorSelecionado.ToString(),
                Email = view.Email,
                Nome = view.Nome
            };

            dataColaborador.Alterar(id, objTela);

            return RedirectToAction("Index", "InicialColaborador");

        }

       
    }
}
