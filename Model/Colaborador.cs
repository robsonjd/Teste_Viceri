﻿using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Colaborador
    {
        [Key]
        public int IdColaborador { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string IdTipo { get; set; }

        public string Cpf { get; set; }

        public string Fl_Ativo { get; set; }
    }
}