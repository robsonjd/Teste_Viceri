﻿using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class TpColaboradores
    {
        [Key]
        public int Id { get; set; }

        public string Tipo { get; set; }

    }
}