﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Commun
{
   public class Cpf
    {
        public static bool CPFValido(string cpf)
        {
            cpf = FormatRemove(cpf);

            if (cpf.Length != 11)
                return false;

            bool vbIgual = true;
            for (int i = 1; i < 11 && vbIgual; i++)
                if (cpf[i] != cpf[0])
                    vbIgual = false;

            if (vbIgual || cpf == "12345678909")
                return false;

            int[] vaNumeros = new int[11];

            for (int i = 0; i < 11; i++)
                vaNumeros[i] = int.Parse(
                cpf[i].ToString());

            int vnSoma = 0;
            for (int i = 0; i < 9; i++)
                vnSoma += (10 - i) * vaNumeros[i];

            int vnResultado = vnSoma % 11;

            if (vnResultado == 1 || vnResultado == 0)
            {
                if (vaNumeros[9] != 0)
                    return false;
            }
            else if (vaNumeros[9] != 11 - vnResultado)
                return false;

            vnSoma = 0;
            for (int i = 0; i < 10; i++)
                vnSoma += (11 - i) * vaNumeros[i];

            vnResultado = vnSoma % 11;

            if (vnResultado == 1 || vnResultado == 0)
            {
                if (vaNumeros[10] != 0)
                    return false;
            }
            else
                if (vaNumeros[10] != 11 - vnResultado)
                return false;

            return true;
        }

        public static string Format(string cpf)
        {
            if (!string.IsNullOrEmpty(cpf))
            {
                var masked = new System.ComponentModel.MaskedTextProvider(@"000\.000\.000-00");
                masked.Set(FormatRemove(cpf));

                return masked.ToString();
            }
            else
                return cpf;
        }

        public static string FormatRemove(string cpf)
        {
            if (!string.IsNullOrEmpty(cpf))
                return cpf.Replace(".", "").Replace("-", "");
            else
                return cpf;
        }

    }
}
