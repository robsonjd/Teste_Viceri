﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ListagemColaborador
    {
        public int IdColaborador { get; set; }

        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Tipo do Colaborador")]
        public string IdTipo { get; set; }

        [Display(Name = "Cpf")]
        public string Cpf { get; set; }

        public string Fl_Ativo { get; set; }

    }
}
